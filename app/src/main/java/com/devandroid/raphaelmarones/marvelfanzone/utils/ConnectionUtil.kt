package com.devandroid.raphaelmarones.marvelfanzone.utils

import java.io.IOException

class ConnectionUtil {

    companion object {
        /**
         * Evaluates Network Connection
         */
        @JvmStatic fun isNetworkAvailable(t: Throwable): Boolean {
            return t is IOException
        }
    }
}
