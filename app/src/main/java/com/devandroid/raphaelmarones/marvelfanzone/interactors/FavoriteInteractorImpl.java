package com.devandroid.raphaelmarones.marvelfanzone.interactors;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.managers.DataPersistanceManager;

import java.util.List;

public class FavoriteInteractorImpl implements FavoriteInteractor {

    @Override
    public List<MarvelCharacters> getFavoriteList(Context mContext) {
        return DataPersistanceManager.getInstance().getFavoriteContent(mContext);
    }

    @Override
    public void saveFavoriteCharacters(List<MarvelCharacters> marvelCharacters, Context mContext) {
        DataPersistanceManager.getInstance().saveFavoriteList(marvelCharacters, mContext);
    }

    @Override
    public void saveDetailedItem(MarvelCharacters item, Context mContext) {
        DataPersistanceManager.getInstance().saveDetailedItem(item, mContext);
    }
}
