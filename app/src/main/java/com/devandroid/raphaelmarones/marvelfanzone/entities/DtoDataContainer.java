package com.devandroid.raphaelmarones.marvelfanzone.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DtoDataContainer<T> {

    @SerializedName("total")
    int total;

    @SerializedName("offset")
    int offset;

    @SerializedName("results")
    List<T> results;

    public List<T> getResults() {
        return results;
    }
    public void setResults(List<T> results) {
        this.results = results;
    }

    public int getTotal() {
        return total;
    }
    public void setTotal(int total) {
        this.total = total;
    }

    public int getOffset() {
        return offset;
    }
    public void setOffset(int offset) {
        this.offset = offset;
    }
}
