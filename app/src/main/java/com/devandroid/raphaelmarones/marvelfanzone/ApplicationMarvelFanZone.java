package com.devandroid.raphaelmarones.marvelfanzone;

import android.app.Application;

import timber.log.Timber;

public class ApplicationMarvelFanZone extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        setupTimber();
    }

    private void setupTimber() {
        Timber.uprootAll();
        Timber.plant(new Timber.DebugTree());
    }

}