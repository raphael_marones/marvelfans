package com.devandroid.raphaelmarones.marvelfanzone.interactors;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelComic;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelEvent;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelSeries;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelStory;

import java.util.List;

public interface DetailedInteractor {

    MarvelCharacters getDetailedItemSaved();
    void findComics(int id, DetailedInteractorListener listener, Context mContext);
    void findEvents(int id, DetailedInteractorListener listener, Context mContext);
    void findSeries(int id, DetailedInteractorListener listener, Context mContext);
    void findStories(int id, DetailedInteractorListener listener, Context mContext);
    void ToggleFavoriteOnDetailedItem(Context mContext);

    interface DetailedInteractorListener {
        void onComicsFound(List<MarvelComic> results);
        void onComicsNotFound();

        void onEventsFound(List<MarvelEvent> results);
        void onEventsNotFound();

        void onSeriesFound(List<MarvelSeries> results);
        void onSeriesNotFound();

        void onStoriesFound(List<MarvelStory> results);
        void onStoriesNotFound();
    }

}
