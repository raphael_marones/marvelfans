package com.devandroid.raphaelmarones.marvelfanzone.ui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.AdapterExtraContentBinding;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelSeries;

import java.util.List;


public class SeriesAdapter extends RecyclerView.Adapter<SeriesAdapter.ExtraViewHolder>{

    private LayoutInflater mLayoutInflater;
    private List<MarvelSeries> mSeriesList;
    private Context mContext;

    public SeriesAdapter(List<MarvelSeries> list, Context context) {
        mSeriesList = list;
        mContext = context;
    }


    @NonNull
    @Override
    public SeriesAdapter.ExtraViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(viewGroup.getContext());
        }

        AdapterExtraContentBinding binding =
                DataBindingUtil.inflate(mLayoutInflater, R.layout.adapter_extra_content, viewGroup, false);

        return new SeriesAdapter.ExtraViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull SeriesAdapter.ExtraViewHolder holder, int i) {
        MarvelSeries item = mSeriesList.get(i);
        holder.binding.extraTitle.setText(item.getTitle());

        if (item.getDescription() != null && !item.getDescription().isEmpty()) holder.binding.extraDescription.setText(item.getDescription());
        else holder.binding.extraDescription.setText(String.format(mContext.getString(R.string.information_not_loaded), item.getTitle()));

    }

    @Override
    public int getItemCount() {
        return mSeriesList.size();
    }

    public class ExtraViewHolder extends RecyclerView.ViewHolder {
        AdapterExtraContentBinding binding;

        public ExtraViewHolder(AdapterExtraContentBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }

}
