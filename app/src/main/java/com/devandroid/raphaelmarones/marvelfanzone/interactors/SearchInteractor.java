package com.devandroid.raphaelmarones.marvelfanzone.interactors;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;

import java.util.List;

public interface SearchInteractor {

    void findHeroesWithName(String query, SearchInteractorListener listener, Context mContext);
    void saveFavoriteCharacters(MarvelCharacters marvelCharacter, Context mContext);
    void saveItemDEtailed(MarvelCharacters marvelCharacter, Context context);

    interface SearchInteractorListener {
        void onHeroesFound(List<MarvelCharacters> results);
        void onGeneralError();
        void onConnectionError();
    }
}
