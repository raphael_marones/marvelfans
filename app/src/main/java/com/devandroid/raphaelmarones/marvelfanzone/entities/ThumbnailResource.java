package com.devandroid.raphaelmarones.marvelfanzone.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ThumbnailResource implements Parcelable {

    @SerializedName("path")
    String path;

    @SerializedName("extension")
    String extension;

    String imagePath;

    public String getImagePath() {
        imagePath = path + "." + extension;
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    protected ThumbnailResource(Parcel in) {
        path = in.readString();
        extension = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(path);
        dest.writeString(extension);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ThumbnailResource> CREATOR = new Creator<ThumbnailResource>() {
        @Override
        public ThumbnailResource createFromParcel(Parcel in) {
            return new ThumbnailResource(in);
        }

        @Override
        public ThumbnailResource[] newArray(int size) {
            return new ThumbnailResource[size];
        }
    };
}
