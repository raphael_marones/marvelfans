package com.devandroid.raphaelmarones.marvelfanzone.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.FragmentSearchBinding;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.SearchPresenter;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.SearchPresenterImpl;
import com.devandroid.raphaelmarones.marvelfanzone.ui.activities.DetailedActivity;
import com.devandroid.raphaelmarones.marvelfanzone.ui.adapters.HeroAdapter;

import java.util.List;


public class SearchFragment extends BaseFragment implements com.devandroid.raphaelmarones.marvelfanzone.views.SearchView, android.support.v7.widget.SearchView.OnQueryTextListener {

    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    private FragmentSearchBinding mBinding;
    private SearchPresenter mEventHandler;
    private HeroAdapter mAdapter;
    private String mQuery;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEventHandler = new SearchPresenterImpl();
        mEventHandler.attachView(this, getActivity().getApplicationContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_search, container, false);
        return mBinding.getRoot();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mQuery = query;

        if (null != mQuery) {
            mEventHandler.onQueryRequested(mQuery);
            hideKeyboard(getActivity());
        }

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mQuery = newText;

        if (null != mQuery) {
            mEventHandler.onQueryRequested(mQuery);
        }

        return true;
    }


    @Override
    public void updateRecyclerView(List<MarvelCharacters> items, List<MarvelCharacters> favoriteCharactersList) {
        mAdapter = new HeroAdapter(items, favoriteCharactersList, getActivity().getApplicationContext());
        mAdapter.setOnMarvelCaracterFavoritePressed(marvelCharacters -> mEventHandler.onMarvelCharacterFavoritePressed(marvelCharacters));
        mAdapter.setOnMarvelCaracterItemPressed(mEventHandler::onItemClicked);
        mBinding.fsearchRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setupContent() {
        mBinding.searchQuery.setQueryHint(getString(R.string.action_search_input));
        mBinding.searchQuery.setIconifiedByDefault(false);
        mBinding.searchQuery.setOnQueryTextListener(this);
        mBinding.searchQuery.setFocusable(true);
        mBinding.searchQuery.requestFocus();
        mBinding.searchQuery.setQuery(mQuery != null ? mQuery : "", false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mBinding.fsearchRecyclerView.setLayoutManager(mLayoutManager);
        mBinding.fsearchRecyclerView.setNestedScrollingEnabled(false);
        mBinding.fsearchRecyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    public void hideLoading() {
        mBinding.privateLoading.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        mBinding.privateLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDetailedActivity() {
        getActivity().startActivity(new Intent(getActivity().getApplicationContext(), DetailedActivity.class));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard(getActivity());
    }

    //PRIVATE METHODS

    private void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();

        if (view == null) {
            view = new View(activity);
        }

        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
