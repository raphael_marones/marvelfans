package com.devandroid.raphaelmarones.marvelfanzone.presenters;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.interactors.FavoriteInteractor;
import com.devandroid.raphaelmarones.marvelfanzone.interactors.FavoriteInteractorImpl;
import com.devandroid.raphaelmarones.marvelfanzone.views.FavoriteView;

import java.util.List;

public class FavoritePresenterImpl implements FavoritePresenter {

    private FavoriteView mFavoriteView;
    private FavoriteInteractor mFavoriteInteractor;
    private Context mContext;

    @Override
    public void attachView(FavoriteView view, Context context) {
        mFavoriteView = view;
        mContext = context;
        mFavoriteInteractor = new FavoriteInteractorImpl();
    }

    private void setupInitialContent() {
        List<MarvelCharacters> favoriteList = mFavoriteInteractor.getFavoriteList(mContext);

        if (favoriteList != null && !favoriteList.isEmpty()) {
            String pageFeedBask = String.format(mContext.getString(R.string.loaded_favorite_feedback), favoriteList.size());
            mFavoriteView.updateMainRecyclerView(favoriteList, pageFeedBask);
        } else
            mFavoriteView.setupFeedBackText(mContext.getString(R.string.loaded_no_favorites_feedback));

    }

    @Override
    public void onMarvelCharacterFavoritePressed(List<MarvelCharacters> marvelCharacters) {
        mFavoriteInteractor.saveFavoriteCharacters(marvelCharacters, mContext);
        String pageFeedBask = mContext.getString(R.string.loaded_no_favorites_feedback);
        if (marvelCharacters != null && !marvelCharacters.isEmpty())
            pageFeedBask = String.format(mContext.getString(R.string.loaded_favorite_feedback), marvelCharacters.size());

        mFavoriteView.setupFeedBackText(pageFeedBask);
    }

    @Override
    public void dettachView() {
        mContext = null;
        mFavoriteView = null;
        mFavoriteInteractor = null;
    }

    @Override
    public void onItemPressed(MarvelCharacters item) {
        mFavoriteInteractor.saveDetailedItem(item, mContext);
        mFavoriteView.showDetailedActivity();
    }

    @Override
    public void refreshFavorites() {
        setupInitialContent();
    }
}
