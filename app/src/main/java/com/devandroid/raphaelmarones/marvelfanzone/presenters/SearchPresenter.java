package com.devandroid.raphaelmarones.marvelfanzone.presenters;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.views.SearchView;

import java.util.List;

public interface SearchPresenter {

    void attachView(SearchView view, Context context);
    void onItemClicked(MarvelCharacters marvelCharacter);
    void onMarvelCharacterFavoritePressed(List<MarvelCharacters> marvelCharacters);
    void onQueryRequested(String mQuery);
}
