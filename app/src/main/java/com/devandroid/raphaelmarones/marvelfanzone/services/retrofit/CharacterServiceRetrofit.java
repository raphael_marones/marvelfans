package com.devandroid.raphaelmarones.marvelfanzone.services.retrofit;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.entities.DtoResponse;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelComic;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelEvent;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelSeries;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelStory;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CharacterServiceRetrofit {

    @GET("characters")
    Call<DtoResponse<MarvelCharacters>> findHeroes(@Query("ts") String timeStamp,
                                                   @Query("hash") String hash,
                                                   @Query("apikey") String apiKey,
                                                   @Query("offset") int offset,
                                                   @Query("limit") int limit);

    @GET("characters")
    Call<DtoResponse<MarvelCharacters>> findHeroesStartingWith(@Query("ts") String timeStamp,
                                                               @Query("hash") String hash,
                                                               @Query("apikey") String apiKey,
                                                               @Query("offset") int offset,
                                                               @Query("limit") int limit,
                                                               @Query("nameStartsWith") String nameStartsWith);

    @GET("characters/{characterId}/comics")
    Call<DtoResponse<MarvelComic>> findComicsByHeroes(@Path("characterId") int characterId,
                                                      @Query("ts") String timeStamp,
                                                      @Query("hash") String hash,
                                                      @Query("apikey") String apiKey,
                                                      @Query("orderBy") String orderBy);

    @GET("characters/{characterId}/events")
    Call<DtoResponse<MarvelEvent>> findEventsByHeroes(@Path("characterId") int characterId,
                                                      @Query("ts") String timeStamp,
                                                      @Query("hash") String hash,
                                                      @Query("apikey") String apiKey);

    @GET("characters/{characterId}/series")
    Call<DtoResponse<MarvelSeries>> findSeriesByHeroes(@Path("characterId") int characterId,
                                                       @Query("ts") String timeStamp,
                                                       @Query("hash") String hash,
                                                       @Query("apikey") String apiKey);

    @GET("characters/{characterId}/stories")
    Call<DtoResponse<MarvelStory>> findStoriesByHeroes(@Path("characterId") int characterId,
                                                       @Query("ts") String timeStamp,
                                                       @Query("hash") String hash,
                                                       @Query("apikey") String apiKey);
}
