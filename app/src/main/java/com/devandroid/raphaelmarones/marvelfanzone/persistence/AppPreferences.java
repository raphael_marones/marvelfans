package com.devandroid.raphaelmarones.marvelfanzone.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.devandroid.raphaelmarones.marvelfanzone.Constants;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.utils.ParseUtil;

import java.util.List;


public class AppPreferences {

    private static AppPreferences instance;
    private SharedPreferences preferences;
    private SharedPreferences.Editor preferencesEditor;

    private AppPreferences(Context thisContext) {
        preferences = PreferenceManager.getDefaultSharedPreferences(thisContext);
        preferencesEditor = preferences.edit();
    }

    public static AppPreferences getInstance(Context context) {
        if (instance == null) {
            synchronized (AppPreferences.class) {
                if (instance == null) instance = new AppPreferences(context);
            }
        }

        return instance;
    }

    public void saveMarvelCharacterList(String key, List<MarvelCharacters> results) {
        String contentToSave = ParseUtil.createMarvelCharacterContent(results);
        preferencesEditor.putString(key, contentToSave);
        preferencesEditor.apply();
    }

    public List<MarvelCharacters> getFavorites() {

        String json = preferences.getString(Constants.DATABASE.FAVORITE_LIST, "");
        if (json == null) return null;

        return ParseUtil.retrieveMarvelCharacters(json);
    }

    public int getMaxPages(String key) {
        return preferences.getInt(key, 10);
    }

    public void saveMaxPages(String key, int mMaxNumberOfPages) {
        preferencesEditor.putInt(key, mMaxNumberOfPages);
        preferencesEditor.apply();
    }

    public List<MarvelCharacters> getMarvelResultsOnKey(int key) {


        String json = preferences.getString(String.valueOf(key), null);
        if (json == null) return null;

        return ParseUtil.retrieveMarvelCharacters(json);
    }
}
