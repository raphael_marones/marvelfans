package com.devandroid.raphaelmarones.marvelfanzone.presenters;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.interactors.SearchInteractor;
import com.devandroid.raphaelmarones.marvelfanzone.interactors.SearchInteractorImpl;
import com.devandroid.raphaelmarones.marvelfanzone.managers.DataPersistanceManager;
import com.devandroid.raphaelmarones.marvelfanzone.ui.adapters.FavoriteAdapter;
import com.devandroid.raphaelmarones.marvelfanzone.views.SearchView;

import java.util.ArrayList;
import java.util.List;

public class SearchPresenterImpl implements SearchPresenter, SearchInteractor.SearchInteractorListener {

    private SearchView mSearchView;
    private SearchInteractor mSearchInteractor;
    private FavoriteAdapter mFavoriteAdapter;
    private Context mContext;

    @Override
    public void attachView(SearchView view, Context context) {
        mContext = context;
        mSearchView = view;
        mSearchInteractor = new SearchInteractorImpl();
        mSearchView.setupContent();
    }

    @Override
    public void onItemClicked(MarvelCharacters marvelCharacter) {
        mSearchInteractor.saveItemDEtailed(marvelCharacter, mContext);
        mSearchView.showDetailedActivity();
    }

    @Override
    public void onMarvelCharacterFavoritePressed(List<MarvelCharacters> marvelCharacters) {
        DataPersistanceManager.getInstance().saveFavoriteList(marvelCharacters, mContext);
    }

    @Override
    public void onQueryRequested(String query) {
        mSearchView.showLoading();
        if (query.isEmpty()) mSearchView.hideLoading();
        mSearchInteractor.findHeroesWithName(query, this, mContext);
    }

    @Override
    public void onHeroesFound(List<MarvelCharacters> results) {
        List<MarvelCharacters> favoriteCharactersList = DataPersistanceManager.getInstance().getFavoriteContent(mContext);
        if (results != null && !results.isEmpty()) {
            mSearchView.updateRecyclerView(results, favoriteCharactersList);
        } else mSearchView.updateRecyclerView(new ArrayList<>(), favoriteCharactersList);

        mSearchView.hideLoading();
    }

    @Override
    public void onGeneralError() {

    }

    @Override
    public void onConnectionError() {

    }
}
