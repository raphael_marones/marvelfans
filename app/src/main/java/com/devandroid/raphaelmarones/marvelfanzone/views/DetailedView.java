package com.devandroid.raphaelmarones.marvelfanzone.views;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelComic;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelEvent;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelSeries;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelStory;

import java.util.List;

public interface DetailedView {

    void setupViewElements(String name, String description, String imagePath);

    void hideComicsLoading();
    void hideComicsSection();
    void updateComicsRecyclerView(List<MarvelComic> list);


    void hideEventsLoading();
    void hideEventsSection();
    void updateEventsRecyclerView(List<MarvelEvent> list);

    void hideSeriesLoading();
    void hideSeriesSection();
    void updateSeriesRecyclerView(List<MarvelSeries> list);

    void hideStoriesLoading();
    void hideStoriesSection();
    void updateStoriesRecyclerView(List<MarvelStory> list);
    void setFavoriteOn();
    void setFavoriteOff();
}
