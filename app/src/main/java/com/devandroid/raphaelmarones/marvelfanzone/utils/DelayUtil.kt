package com.devandroid.raphaelmarones.marvelfanzone.utils

import android.os.Handler

class DelayUtil {

    companion object {

        interface DelayCallback {
            fun afterDelay()
        }

        @JvmStatic fun delay(milliseconds: Int, delayCallback: DelayCallback) {
            val handler = Handler()
            handler.postDelayed({ delayCallback.afterDelay() },
                    milliseconds.toLong())
        }

    }
}

