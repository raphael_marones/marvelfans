package com.devandroid.raphaelmarones.marvelfanzone.presenters;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelComic;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelEvent;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelSeries;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelStory;
import com.devandroid.raphaelmarones.marvelfanzone.interactors.DetailedInteractor;
import com.devandroid.raphaelmarones.marvelfanzone.interactors.DetailedInteractorImpl;
import com.devandroid.raphaelmarones.marvelfanzone.views.DetailedView;

import java.util.List;

public class DetailedPresenterImpl implements DetailedPresenter, DetailedInteractor.DetailedInteractorListener {
    private DetailedView mDetailedView;
    private DetailedInteractor mDetailedInteractor;
    private Context mContext;

    @Override
    public void attachView(DetailedView view, Context context) {
        mContext = context;
        mDetailedView = view;
        mDetailedInteractor = new DetailedInteractorImpl();

        setupContent();
    }

    private void setupContent() {
        MarvelCharacters marvelCharacters = mDetailedInteractor.getDetailedItemSaved();
        int id = marvelCharacters.getId();
        String description = marvelCharacters.getDescription();
        if (description!=null && !description.isEmpty()) mDetailedView.setupViewElements(marvelCharacters.getName(), marvelCharacters.getDescription(), marvelCharacters.getThumbnail().getImagePath());
        else mDetailedView.setupViewElements(marvelCharacters.getName(), String.format(mContext.getString(R.string.label_notfound_at_all), marvelCharacters.getName()), marvelCharacters.getThumbnail().getImagePath());

        mDetailedInteractor.findComics(id, this, mContext);
        mDetailedInteractor.findEvents(id, this, mContext);
        mDetailedInteractor.findSeries(id, this, mContext);
        mDetailedInteractor.findStories(id, this, mContext);
        setOnOfFavoriteAnimation();
    }

    private void setOnOfFavoriteAnimation() {
        MarvelCharacters marvelCharacters = mDetailedInteractor.getDetailedItemSaved();
        if (marvelCharacters.isFavorite()) {
            mDetailedView.setFavoriteOn();
        } else {
            mDetailedView.setFavoriteOff();
        }
    }

    @Override
    public void onMarvelCharacterFavoritePressed() {
        mDetailedInteractor.ToggleFavoriteOnDetailedItem(mContext);
        setOnOfFavoriteAnimation();
    }

    @Override
    public void onComicsFound(List<MarvelComic> results) {
        mDetailedView.updateComicsRecyclerView(results);
        mDetailedView.hideComicsLoading();
    }

    @Override
    public void onComicsNotFound() {
        mDetailedView.hideComicsSection();
    }

    @Override
    public void onEventsFound(List<MarvelEvent> results) {
        mDetailedView.updateEventsRecyclerView(results);
        mDetailedView.hideEventsLoading();
    }

    @Override
    public void onEventsNotFound() {
        mDetailedView.hideEventsSection();
    }

    @Override
    public void onSeriesFound(List<MarvelSeries> results) {
        mDetailedView.updateSeriesRecyclerView(results);
        mDetailedView.hideSeriesLoading();
    }

    @Override
    public void onSeriesNotFound() {
        mDetailedView.hideSeriesSection();
    }

    @Override
    public void onStoriesFound(List<MarvelStory> results) {
        mDetailedView.updateStoriesRecyclerView(results);
        mDetailedView.hideStoriesLoading();
    }

    @Override
    public void onStoriesNotFound() {
        mDetailedView.hideStoriesSection();
    }

}
