package com.devandroid.raphaelmarones.marvelfanzone.services.base;

import com.devandroid.raphaelmarones.marvelfanzone.utils.RetrofitUtil;

public abstract class BaseService {

    public static <T> T buildApiService(Class<T> mClass) {
        return RetrofitUtil.getInstance()
                .apiBuild()
                .create(mClass);
    }
}
