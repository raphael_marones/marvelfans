package com.devandroid.raphaelmarones.marvelfanzone.presenters;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.Constants;
import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.interactors.HeroInteractorImpl;
import com.devandroid.raphaelmarones.marvelfanzone.interactors.HeroInteractor;
import com.devandroid.raphaelmarones.marvelfanzone.managers.LoadPageManager;
import com.devandroid.raphaelmarones.marvelfanzone.views.HeroView;
import java.util.List;

public class HeroPresenterImpl implements HeroPresenter, HeroInteractor.HeroInteractorListener {

    private HeroView mHeroView;
    private HeroInteractor mHeroInteractor;
    private Context mContext;

    @Override
    public void attachView(HeroView view, Context context) {
        mHeroView = view;
        mContext = context;
        mHeroInteractor = new HeroInteractorImpl();
        setupRecyclerView();
        setupInitivalContent();
    }

    private void setupInitivalContent() {
        mHeroView.showLoading(R.string.loading_information);
        mHeroInteractor.findHeroes(0, 20, this, mContext);
    }

    private void setupRecyclerView() {
        mHeroView.setupView();
    }

    @Override
    public void onItemClicked(MarvelCharacters marvelCharacters) {
        mHeroInteractor.saveDetailedItem(marvelCharacters, mContext);
        mHeroView.showDetailedActivity();
    }

    @Override
    public void onPreviousPageButtonPressed() {
        mHeroView.showLoading(R.string.loading_information);
        int page = LoadPageManager.getInstance().getPreviousPage();
        mHeroInteractor.findHeroes(page, Constants.REQUEST.NUMBER_OF_PAGES, this, mContext);
    }

    @Override
    public void onNextPageButtonPressed() {
        mHeroView.showLoading(R.string.loading_information);
        int page = LoadPageManager.getInstance().getNextPage();
        mHeroInteractor.findHeroes(page, Constants.REQUEST.NUMBER_OF_PAGES, this, mContext);
    }

    @Override
    public void onMarvelCharacterFavoritePressed(List<MarvelCharacters> marvelCharacters) {
        mHeroInteractor.saveFavoriteCharacters(marvelCharacters, mContext);
    }

    @Override
    public void onFavoriteRefreshRequested() {
        mHeroView.showLoading(R.string.loading_information);
        mHeroInteractor.findHeroes(LoadPageManager.getInstance().geturrentPage(), Constants.REQUEST.NUMBER_OF_PAGES, this, mContext);
    }

    @Override
    public void onHeroesFound(int page, List<MarvelCharacters> results, boolean isLastPage) {
        int minRange = (page - 1) * Constants.REQUEST.NUMBER_OF_PAGES + 1;
        int maxRange = minRange + results.size() -1;
        String pageFeedBack = String.format(mContext.getString(R.string.loaded_feedback), minRange, maxRange);

        mHeroView.updateMainRecyclerView(results , pageFeedBack, mHeroInteractor.getFavoriteList(mContext));
        mHeroView.hideLoading();
    }

    @Override
    public void onGeneralError() {
        mHeroView.hideLoading();
        LoadPageManager.getInstance().getOldPage();
    }

    @Override
    public void onConnectionError() {
        mHeroView.hideLoading();
        LoadPageManager.getInstance().getOldPage();
    }

    @Override
    public void onLastPageBeingLoaded() {
        mHeroView.showLoading(R.string.loading_information_last_page);
    }

    @Override
    public void onLastPageAlreadyLoaded() {
        mHeroView.hideLoading();
        mHeroView.setupWarningToast(R.string.loading_information_last_page_previously_loaded);
    }
}
