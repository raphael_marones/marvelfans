package com.devandroid.raphaelmarones.marvelfanzone.views;

public interface MainView {
    void setupViewElements();
    void setupInitialFragment();
}
