package com.devandroid.raphaelmarones.marvelfanzone.interactors;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.Constants;
import com.devandroid.raphaelmarones.marvelfanzone.entities.DtoDataContainer;
import com.devandroid.raphaelmarones.marvelfanzone.entities.DtoResponse;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.managers.DataPersistanceManager;
import com.devandroid.raphaelmarones.marvelfanzone.managers.LoadPageManager;
import com.devandroid.raphaelmarones.marvelfanzone.services.CharacterService;
import com.devandroid.raphaelmarones.marvelfanzone.services.base.BaseCallback;
import com.devandroid.raphaelmarones.marvelfanzone.utils.ConnectionUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class HeroInteractorImpl implements HeroInteractor {

    private int atempt = 0;

    @Override
    public void findHeroes(int page, int limit, HeroInteractorListener listener, Context context) {
        LoadPageManager.getInstance().saveLastPageRequested(page);
        int numberOfReturnsPerPage = Constants.REQUEST.NUMBER_OF_PAGES;
        int offset = page * numberOfReturnsPerPage;
        int offLinePage = page + 1;
        List offLineResults = DataPersistanceManager.getInstance().getPageContent(offLinePage, context);

        if (page == LoadPageManager.getInstance().getMaxNumberOfPages(context) - 1) listener.onLastPageBeingLoaded();

        if (page == LoadPageManager.getInstance().getMaxNumberOfPages(context)) {
            listener.onLastPageAlreadyLoaded();
            LoadPageManager.getInstance().getPreviousPage();
            return;
        }

        if (offLineResults != null && !offLineResults.isEmpty()) {
            listener.onHeroesFound(offLinePage, updatedList(offLineResults, context), false);
            return;
        }

        CharacterService.findHeroes(offset, limit, new BaseCallback() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    DtoResponse dtoCharacterHeroesList = (DtoResponse) response.body();
                    DtoDataContainer dtoDataContainer = dtoCharacterHeroesList.getData();
                    LoadPageManager.getInstance().setMaxNumberOfPages(dtoDataContainer.getTotal(), context);
                    List results = dtoDataContainer.getResults();
                    int page = (dtoDataContainer.getOffset() + numberOfReturnsPerPage) / numberOfReturnsPerPage;
                    boolean isLastPage =  (dtoDataContainer.getTotal() - offset) <= numberOfReturnsPerPage;
                    DataPersistanceManager.getInstance().persistResults(results, page, context);

                    if (page == LoadPageManager.getInstance().getLastPageRequested() + 1) listener.onHeroesFound(page, updatedList(results, context), isLastPage);
                    atempt = 0;

                } catch (Exception e) {
                    atempt += 1;
                    Timber.e("[HeroInteractorImpl] - findHeroes -  onResponse : " + e.getMessage());

                    if (atempt < 3) {
                        findHeroes(page, limit, listener, context);
                        return;
                    }

                    listener.onGeneralError();
                    atempt = 0;
                    Timber.e("[HeroInteractorImpl] - findHeroes - onResponse : " + e.getMessage());
                }
            }

            @Override
            public void onGeneralError(Call call, Throwable t) {
                if (ConnectionUtil.isNetworkAvailable(t)) {
                    listener.onConnectionError();
                    Timber.d("[HeroInteractorImpl] - findHeroes - onConnectionError : " + t.getMessage());
                    return;
                }

                atempt += 1;
                Timber.e("[HeroInteractorImpl] - findHeroes - onGeneralError : " + t.getMessage());

                if (atempt < 3) {
                    findHeroes(page, limit, listener, context);
                    return;
                }

                listener.onGeneralError();
                atempt = 0;

            }
        });
    }

    private List<MarvelCharacters> updatedList(List<MarvelCharacters> results, Context context) {
        List<MarvelCharacters> updatedList = new ArrayList<>();
        List<MarvelCharacters> favoriteList = DataPersistanceManager.getInstance().getFavoriteContent(context);

        for (MarvelCharacters marvelCharacter : results){
            for (MarvelCharacters fav : favoriteList){
                if (marvelCharacter.getId() == fav.getId()) marvelCharacter.setFavorite(true);
            }
            updatedList.add(marvelCharacter);
        }

        return updatedList;
    }

    @Override
    public List<MarvelCharacters> getFavoriteList(Context mContext) {
        return DataPersistanceManager.getInstance().getFavoriteContent(mContext);
    }

    @Override
    public void saveFavoriteCharacters(List<MarvelCharacters> marvelCharacters, Context mContext) {
        DataPersistanceManager.getInstance().saveFavoriteList(marvelCharacters, mContext);
    }

    @Override
    public void saveDetailedItem(MarvelCharacters marvelCharacters, Context context) {
        DataPersistanceManager.getInstance().saveDetailedItem(marvelCharacters, context);
    }
}
