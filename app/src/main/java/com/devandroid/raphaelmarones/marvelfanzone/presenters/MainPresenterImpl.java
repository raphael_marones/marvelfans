package com.devandroid.raphaelmarones.marvelfanzone.presenters;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.interactors.HeroInteractor;
import com.devandroid.raphaelmarones.marvelfanzone.interactors.HeroInteractorImpl;
import com.devandroid.raphaelmarones.marvelfanzone.views.MainView;


public class MainPresenterImpl implements MainPresenter {

    private MainView mMainView;
    private Context mContext;
    private HeroInteractor mHeroInteractor;

    @Override
    public void attachView(MainView view, Context context) {
        mContext = context;
        mMainView = view;
        mHeroInteractor = new HeroInteractorImpl();
        mMainView.setupViewElements();
        setupInitialContent();
    }


    private void setupInitialContent() {
        mMainView.setupInitialFragment();
    }

}
