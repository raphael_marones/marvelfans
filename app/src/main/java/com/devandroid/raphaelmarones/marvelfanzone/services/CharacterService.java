package com.devandroid.raphaelmarones.marvelfanzone.services;

import com.devandroid.raphaelmarones.marvelfanzone.BuildConfig;
import com.devandroid.raphaelmarones.marvelfanzone.Constants;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.entities.DtoResponse;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelComic;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelEvent;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelSeries;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelStory;
import com.devandroid.raphaelmarones.marvelfanzone.services.base.BaseCallback;
import com.devandroid.raphaelmarones.marvelfanzone.services.base.BaseService;
import com.devandroid.raphaelmarones.marvelfanzone.services.retrofit.CharacterServiceRetrofit;
import com.devandroid.raphaelmarones.marvelfanzone.utils.HashGeneratorUtil;
import com.devandroid.raphaelmarones.marvelfanzone.utils.RetrofitUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterService extends BaseService {

    public static void findHeroes(int offset, int limit, BaseCallback callback){
        final CharacterServiceRetrofit characterServiceRetrofit = RetrofitUtil.getInstance().apiBuild().create(CharacterServiceRetrofit.class);
        String ts = String.valueOf(System.currentTimeMillis());
        String hash = HashGeneratorUtil.generateHash(System.currentTimeMillis(), BuildConfig.PRIVATE_KEY, BuildConfig.PUBLIC_KEY);
        final Call<DtoResponse<MarvelCharacters>> call = characterServiceRetrofit.findHeroes(ts, hash , BuildConfig.PUBLIC_KEY, offset, limit);

        call.enqueue(new Callback<DtoResponse<MarvelCharacters>>() {
            @Override
            public void onResponse(Call<DtoResponse<MarvelCharacters>> call, Response<DtoResponse<MarvelCharacters>> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<DtoResponse<MarvelCharacters>> call, Throwable t) {
                callback.onGeneralError(call, t);
            }
        });
    }

    public static void findHeroesQuerySearch(String query, BaseCallback callback){
        final CharacterServiceRetrofit characterServiceRetrofit = RetrofitUtil.getInstance().apiBuild().create(CharacterServiceRetrofit.class);
        String ts = String.valueOf(System.currentTimeMillis());
        String hash = HashGeneratorUtil.generateHash(System.currentTimeMillis(), BuildConfig.PRIVATE_KEY, BuildConfig.PUBLIC_KEY);

        final Call<DtoResponse<MarvelCharacters>> call = characterServiceRetrofit.findHeroesStartingWith(ts, hash , BuildConfig.PUBLIC_KEY, Constants.REQUEST.NUMBER_OF_PAGES , Constants.REQUEST.MAX_RESULTS, query);

        call.enqueue(new Callback<DtoResponse<MarvelCharacters>>() {
            @Override
            public void onResponse(Call<DtoResponse<MarvelCharacters>> call, Response<DtoResponse<MarvelCharacters>> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<DtoResponse<MarvelCharacters>> call, Throwable t) {
                callback.onGeneralError(call, t);
            }
        });
    }


    public static void findComicsByHeroes(int id, BaseCallback callback){
        final CharacterServiceRetrofit characterServiceRetrofit = RetrofitUtil.getInstance().apiBuild().create(CharacterServiceRetrofit.class);
        String ts = String.valueOf(System.currentTimeMillis());
        String hash = HashGeneratorUtil.generateHash(System.currentTimeMillis(), BuildConfig.PRIVATE_KEY, BuildConfig.PUBLIC_KEY);

        final Call<DtoResponse<MarvelComic>> call = characterServiceRetrofit.findComicsByHeroes(id, ts, hash , BuildConfig.PUBLIC_KEY, Constants.REQUEST.ORDER_BY);

        call.enqueue(new Callback<DtoResponse<MarvelComic>>() {
            @Override
            public void onResponse(Call<DtoResponse<MarvelComic>> call, Response<DtoResponse<MarvelComic>> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<DtoResponse<MarvelComic>> call, Throwable t) {
                callback.onGeneralError(call, t);
            }
        });
    }

    public static void findEventsByHeroes(int id, BaseCallback callback){
        final CharacterServiceRetrofit characterServiceRetrofit = RetrofitUtil.getInstance().apiBuild().create(CharacterServiceRetrofit.class);
        String ts = String.valueOf(System.currentTimeMillis());
        String hash = HashGeneratorUtil.generateHash(System.currentTimeMillis(), BuildConfig.PRIVATE_KEY, BuildConfig.PUBLIC_KEY);

        final Call<DtoResponse<MarvelEvent>> call = characterServiceRetrofit.findEventsByHeroes(id, ts, hash , BuildConfig.PUBLIC_KEY);

        call.enqueue(new Callback<DtoResponse<MarvelEvent>>() {
            @Override
            public void onResponse(Call<DtoResponse<MarvelEvent>> call, Response<DtoResponse<MarvelEvent>> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<DtoResponse<MarvelEvent>> call, Throwable t) {
                callback.onGeneralError(call, t);
            }
        });
    }

    public static void findSeriesByHeroes(int id, BaseCallback callback){
        final CharacterServiceRetrofit characterServiceRetrofit = RetrofitUtil.getInstance().apiBuild().create(CharacterServiceRetrofit.class);
        String ts = String.valueOf(System.currentTimeMillis());
        String hash = HashGeneratorUtil.generateHash(System.currentTimeMillis(), BuildConfig.PRIVATE_KEY, BuildConfig.PUBLIC_KEY);

        final Call<DtoResponse<MarvelSeries>> call = characterServiceRetrofit.findSeriesByHeroes(id, ts, hash , BuildConfig.PUBLIC_KEY);

        call.enqueue(new Callback<DtoResponse<MarvelSeries>>() {
            @Override
            public void onResponse(Call<DtoResponse<MarvelSeries>> call, Response<DtoResponse<MarvelSeries>> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<DtoResponse<MarvelSeries>> call, Throwable t) {
                callback.onGeneralError(call, t);
            }
        });
    }

    public static void findStoriesByHeroes(int id, BaseCallback callback){
        final CharacterServiceRetrofit characterServiceRetrofit = RetrofitUtil.getInstance().apiBuild().create(CharacterServiceRetrofit.class);
        String ts = String.valueOf(System.currentTimeMillis());
        String hash = HashGeneratorUtil.generateHash(System.currentTimeMillis(), BuildConfig.PRIVATE_KEY, BuildConfig.PUBLIC_KEY);

        final Call<DtoResponse<MarvelStory>> call = characterServiceRetrofit.findStoriesByHeroes(id, ts, hash , BuildConfig.PUBLIC_KEY);

        call.enqueue(new Callback<DtoResponse<MarvelStory>>() {
            @Override
            public void onResponse(Call<DtoResponse<MarvelStory>> call, Response<DtoResponse<MarvelStory>> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<DtoResponse<MarvelStory>> call, Throwable t) {
                callback.onGeneralError(call, t);
            }
        });
    }
}
