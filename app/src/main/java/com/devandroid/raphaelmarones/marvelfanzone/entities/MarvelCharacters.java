package com.devandroid.raphaelmarones.marvelfanzone.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MarvelCharacters implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("urls")
    private List<Urls> urls;

    @SerializedName("thumbnail")
    private ThumbnailResource thumbnail;

    @SerializedName("comics")
    private DtoExtraContents<MarvelComicSummary> marvelComicSummary;

    @SerializedName("stories")
    private DtoExtraContents<MarvelStorySummary> marvelStorySummary;

    @SerializedName("events")
    private DtoExtraContents<MarvelEventSummary> marvelEventSummary;

    @SerializedName("series")
    private DtoExtraContents<MarvelSeriesSummary> marvelSeriesSummary;

    public DtoExtraContents<MarvelComicSummary> getMarvelComicSummary() {
        return marvelComicSummary;
    }

    public void setMarvelComicSummary(DtoExtraContents<MarvelComicSummary> marvelComicSummary) {
        this.marvelComicSummary = marvelComicSummary;
    }

    public DtoExtraContents<MarvelStorySummary> getMarvelStorySummary() {
        return marvelStorySummary;
    }

    public void setMarvelStorySummary(DtoExtraContents<MarvelStorySummary> marvelStorySummary) {
        this.marvelStorySummary = marvelStorySummary;
    }

    public DtoExtraContents<MarvelEventSummary> getMarvelEventSummary() {
        return marvelEventSummary;
    }

    public void setMarvelEventSummary(DtoExtraContents<MarvelEventSummary> marvelEventSummary) {
        this.marvelEventSummary = marvelEventSummary;
    }

    public DtoExtraContents<MarvelSeriesSummary> getMarvelSeriesSummary() {
        return marvelSeriesSummary;
    }

    public void setMarvelSeriesSummary(DtoExtraContents<MarvelSeriesSummary> marvelSeriesSummary) {
        this.marvelSeriesSummary = marvelSeriesSummary;
    }

    private boolean isFavorite;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Urls> getUrls() {
        return urls;
    }

    public void setUrls(List<Urls> urls) {
        this.urls = urls;
    }

    public ThumbnailResource getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ThumbnailResource thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeTypedList(urls);
        dest.writeParcelable(thumbnail, flags);
        dest.writeByte((byte) (isFavorite ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MarvelCharacters> CREATOR = new Creator<MarvelCharacters>() {
        @Override
        public MarvelCharacters createFromParcel(Parcel in) {
            return new MarvelCharacters(in);
        }

        @Override
        public MarvelCharacters[] newArray(int size) {
            return new MarvelCharacters[size];
        }
    };

    protected MarvelCharacters(Parcel in) {
        id = in.readInt();
        name = in.readString();
        description = in.readString();
        urls = in.createTypedArrayList(Urls.CREATOR);
        thumbnail = in.readParcelable(ThumbnailResource.class.getClassLoader());
        isFavorite = in.readByte() != 0;
    }
}
