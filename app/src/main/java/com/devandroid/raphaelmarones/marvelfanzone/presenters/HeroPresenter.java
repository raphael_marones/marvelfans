package com.devandroid.raphaelmarones.marvelfanzone.presenters;


import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.views.HeroView;

import java.util.List;

public interface HeroPresenter {

    void attachView(HeroView view, Context context);
    void onItemClicked(MarvelCharacters marvelCharacters);
    void onPreviousPageButtonPressed();
    void onNextPageButtonPressed();
    void onMarvelCharacterFavoritePressed(List<MarvelCharacters> marvelCharacters);
    void onFavoriteRefreshRequested();
}
