package com.devandroid.raphaelmarones.marvelfanzone.presenters;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.views.MainView;

public interface MainPresenter {
    void attachView(MainView view, Context context);
}
