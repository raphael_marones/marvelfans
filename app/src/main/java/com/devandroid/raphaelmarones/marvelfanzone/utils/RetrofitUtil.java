package com.devandroid.raphaelmarones.marvelfanzone.utils;

import com.devandroid.raphaelmarones.marvelfanzone.BuildConfig;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtil {

    public static RetrofitUtil instance;

    private RetrofitUtil() {
    }

    public static RetrofitUtil getInstance() {
        if (instance == null) {
            instance = new RetrofitUtil();
        }

        return instance;
    }

    public Retrofit apiBuild() {
        final OkHttpClient client = client();

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder()))
                .client(client)
                .build();
    }

    // Private

    private OkHttpClient client() {
        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);

        httpClient.addInterceptor(chain -> {
            final Request original = chain.request();
            final String version = BuildConfig.VERSION_NAME;

            final Request request = original.newBuilder()
                    .build();

            return chain.proceed(request);
        });


        return httpClient.build();
    }

    private Gson gsonBuilder() {
        final FieldNamingStrategy customPolicy = f -> f.getName().toLowerCase();
        return new GsonBuilder()
                .setFieldNamingStrategy(customPolicy)
                .create();
    }
}
