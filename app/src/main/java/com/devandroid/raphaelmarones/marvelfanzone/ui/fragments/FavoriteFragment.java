package com.devandroid.raphaelmarones.marvelfanzone.ui.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.FragmentFavoriteBinding;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.FavoritePresenter;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.FavoritePresenterImpl;
import com.devandroid.raphaelmarones.marvelfanzone.ui.activities.DetailedActivity;
import com.devandroid.raphaelmarones.marvelfanzone.ui.adapters.FavoriteAdapter;
import com.devandroid.raphaelmarones.marvelfanzone.views.FavoriteView;

import java.util.List;

public class FavoriteFragment extends BaseFragment implements FavoriteView {

    private FragmentFavoriteBinding binding;
    private FavoritePresenter mEventHandler;

    public static Fragment newInstance(){
        return new FavoriteFragment();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_favorite, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEventHandler = new FavoritePresenterImpl();
        mEventHandler.attachView(this, getActivity().getApplicationContext());
        binding.favoriteRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onResume() {
        super.onResume();
        mEventHandler.refreshFavorites();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mEventHandler.dettachView();
    }

    @Override
    public void updateMainRecyclerView(List<MarvelCharacters> list, String pageFeedBack) {
        FavoriteAdapter adapter = new FavoriteAdapter(list, getActivity().getApplicationContext());
        adapter.setOnMarvelCaracterFavoritePressed(marvelCharacters -> mEventHandler.onMarvelCharacterFavoritePressed(marvelCharacters));
        adapter.setOnItemPressed(mEventHandler::onItemPressed);

        binding.favoriteRecyclerView.setAdapter(adapter);
        binding.favoriteInformation.setText(pageFeedBack);
    }

    @Override
    public void setupFeedBackText(String pageFeedBask) {
        binding.favoriteInformation.setText(pageFeedBask);
    }

    @Override
    public void showDetailedActivity() {
        getActivity().startActivity(new Intent(getActivity().getApplicationContext(), DetailedActivity.class));
    }
}
