package com.devandroid.raphaelmarones.marvelfanzone.entities;

import com.google.gson.annotations.SerializedName;

public class DtoResponse<T> {

    private int code;
    private String status;

    @SerializedName("data")
    private DtoDataContainer<T> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DtoDataContainer<T> getData() {
        return data;
    }

    public void setData(DtoDataContainer<T> data) {
        this.data = data;
    }


}
