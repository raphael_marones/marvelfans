package com.devandroid.raphaelmarones.marvelfanzone.ui.fragments;

import android.app.Fragment;
import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.IActivityCallback;


public abstract class BaseFragment extends Fragment {

    protected IActivityCallback mActivityCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mActivityCallback = (IActivityCallback) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mActivityCallback = null;
    }


}
