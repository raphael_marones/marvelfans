package com.devandroid.raphaelmarones.marvelfanzone;

public interface Constants {

    interface ENVIRONMENT {
        boolean STUB_MODE                                                   = false;
        int SPLASH_DURATION                                                 = 2000;
    }

    interface GENERAL_ID {
        String UNKNOWN                                                      = "unknown";
        String FRAGMENT_HEROES                                              = "FRAGMENT_HEROES";
        String FRAGMENT_FAVORITED_HEROES                                    = "FRAGMENT_FAVORITED_HEROES";
        String ACTIVITY_ROOT                                                = "ACTIVITY_ROOT";
    }

    interface DATABASE {
        int VERSION                                                         = 1;
        String NAME                                                         = "MARVELFANZONE_DB";
        String FAVORITE_LIST                                                = "MARVELFANZONE_FAVORITES";
        String PAGES                                                        = "MARVELFANZONE_MAXPAGES";
    }

    interface REQUEST {
        int NUMBER_OF_PAGES                                                 = 20;
        int MAX_RESULTS                                                     = 100;
        String ORDER_BY                                                     = "onsaleDate";
        int MIN_RESULTS                                                     = 10;
    }

    interface ANIMATION {
        int DURATION_OFFSET                                                 = 100;
        int DURATION_1X                                                     = 200;
        int DURATION_2X                                                     = 400;
        int DURATION_3X                                                     = 600;
    }
}
