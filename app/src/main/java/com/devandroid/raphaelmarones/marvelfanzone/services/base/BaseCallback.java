package com.devandroid.raphaelmarones.marvelfanzone.services.base;

import retrofit2.Call;
import retrofit2.Response;

public interface BaseCallback<T> {

    void onResponse(Call<T> call, Response<T> response);

    void onGeneralError(Call<T> call, Throwable t);
}
