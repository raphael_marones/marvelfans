package com.devandroid.raphaelmarones.marvelfanzone.ui.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.bumptech.glide.Glide;
import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.ActivityDetailedBinding;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelComic;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelEvent;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelSeries;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelStory;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.DetailedPresenter;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.DetailedPresenterImpl;
import com.devandroid.raphaelmarones.marvelfanzone.ui.adapters.ComicAdapter;
import com.devandroid.raphaelmarones.marvelfanzone.ui.adapters.EventAdapter;
import com.devandroid.raphaelmarones.marvelfanzone.ui.adapters.SeriesAdapter;
import com.devandroid.raphaelmarones.marvelfanzone.ui.adapters.StoryAdapter;
import com.devandroid.raphaelmarones.marvelfanzone.utils.AnimationUtil;
import com.devandroid.raphaelmarones.marvelfanzone.views.DetailedView;

import java.util.List;

import timber.log.Timber;

public class DetailedActivity extends AppCompatActivity implements DetailedView {

    private ActivityDetailedBinding mBinding;
    private DetailedPresenter mEventHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detailed);
        mEventHandler = new DetailedPresenterImpl();
        mEventHandler.attachView(this, this);
    }

    @Override
    public void setupViewElements(String name, String description, String imagePath) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        mBinding.heroName.setText(name);
        mBinding.heroDescription.setText(description);

        mBinding.favoriteImageButton.setOnClickListener(view -> {
            mEventHandler.onMarvelCharacterFavoritePressed();
        });

        try {
            if (imagePath != null && !imagePath.contains("image_not_available"))
                Glide.with(this)
                        .load(imagePath)
                        .into(mBinding.heroImageDetailed);
        } catch (Exception e) {
            Timber.e("[DetailedActivity] - image loading failed : " + e.getMessage());
        }

        // todo WORK ON SHARED ELEMENT ANIMATION
    }

    @Override
    public void hideComicsLoading() {
        mBinding.comicsLoading.setVisibility(View.GONE);
    }

    @Override
    public void hideComicsSection() {
        mBinding.sectionComics.setVisibility(View.GONE);
    }

    @Override
    public void updateComicsRecyclerView(List<MarvelComic> list) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.comicsRecyclerView.setLayoutManager(mLayoutManager);
        mBinding.comicsRecyclerView.setNestedScrollingEnabled(false);
        mBinding.comicsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        ComicAdapter adapter = new ComicAdapter(list, this);
        mBinding.comicsRecyclerView.setAdapter(adapter);
    }

    @Override
    public void hideEventsLoading() {
        mBinding.eventsLoading.setVisibility(View.GONE);
    }

    @Override
    public void hideEventsSection() {
        mBinding.sectionEvents.setVisibility(View.GONE);
    }

    @Override
    public void updateEventsRecyclerView(List<MarvelEvent> list) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.eventsRecyclerView.setLayoutManager(mLayoutManager);
        EventAdapter adapter = new EventAdapter(list, this);
        mBinding.eventsRecyclerView.setAdapter(adapter);
    }
    @Override
    public void hideSeriesLoading() {
        mBinding.seriesLoading.setVisibility(View.GONE);
    }

    @Override
    public void hideSeriesSection() {
        mBinding.sectionSeries.setVisibility(View.GONE);
    }

    @Override
    public void updateSeriesRecyclerView(List<MarvelSeries> list) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.seriesRecyclerView.setLayoutManager(mLayoutManager);
        SeriesAdapter adapter = new SeriesAdapter(list, this);
        mBinding.seriesRecyclerView.setAdapter(adapter);
    }

    @Override
    public void hideStoriesLoading() {
        mBinding.storiesLoading.setVisibility(View.GONE);
    }

    @Override
    public void hideStoriesSection() {
        mBinding.sectionStories.setVisibility(View.GONE);
    }

    @Override
    public void updateStoriesRecyclerView(List<MarvelStory> list) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.storiesRecyclerView.setLayoutManager(mLayoutManager);
        StoryAdapter adapter = new StoryAdapter(list, this);
        mBinding.storiesRecyclerView.setAdapter(adapter);
    }

    @Override
    public void setFavoriteOn() {
        AnimationUtil.setCrossFadeAnimation(null, mBinding.favoriteImageOn);
    }

    @Override
    public void setFavoriteOff() {
        AnimationUtil.setCrossFadeAnimation(mBinding.favoriteImageOn, null);
    }
}
