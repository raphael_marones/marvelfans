package com.devandroid.raphaelmarones.marvelfanzone.interactors;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;

import java.util.List;

public interface FavoriteInteractor {

    List<MarvelCharacters> getFavoriteList(Context mContext);
    void saveFavoriteCharacters(List<MarvelCharacters> marvelCharacters, Context mContext);
    void saveDetailedItem(MarvelCharacters item, Context mContext);
}
