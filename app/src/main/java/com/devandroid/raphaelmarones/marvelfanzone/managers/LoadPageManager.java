package com.devandroid.raphaelmarones.marvelfanzone.managers;


import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.Constants;

public class LoadPageManager {

    private static LoadPageManager instance;
    private int mFormerPage;
    private int mCurrentPage = 0;
    private int mLastPageRequested = 1;
    private int mMaxNumberOfPages = 10;

    public static LoadPageManager getInstance() {

        if (instance == null) {
            synchronized (LoadPageManager.class) {
                if (instance == null) instance = new LoadPageManager();
            }
        }

        return instance;
    }

    /**
     * Retrieves number of page loaded
     *
     * @return int value representing current page
     */
    public int geturrentPage() {
        return mCurrentPage;
    }

    /**
     * Retrieves number of page to be loaded
     *
     * @return int the int value representing page to be loaded
     */
    public int getNextPage() {
        mFormerPage = mCurrentPage;
        mCurrentPage += 1;
        return mCurrentPage;
    }

    /**
     * Retrieves number of page to be loaded
     *
     * @return int the int value representing page to be loaded
     */
    public int getPreviousPage() {
        if (mCurrentPage > 0) {
            mFormerPage = mCurrentPage;
            mCurrentPage -= 1;
        }
        return mCurrentPage;
    }


    /**
     * Retrieves number of page previously loaded
     *
     * @return int the int value representing page previously loaded
     */
    public void getOldPage() {
        mCurrentPage = mFormerPage;
    }


    /**
     * Saves data in correct reference of page
     *
     * @param page the int value representing page
     */
    public void saveLastPageRequested(int page) {
        mLastPageRequested = page;
    }

    /**
     * Saves page requested to API
     *
     * @return the int value representing page  requested to Api
     */
    public int getLastPageRequested() {
        return mLastPageRequested;
    }

    /**
     * Sets max number of pages retrieved from API
     *
     * @param total   Number of Items
     * @param context
     */
    public void setMaxNumberOfPages(int total, Context context) {
        mMaxNumberOfPages = (total / Constants.REQUEST.NUMBER_OF_PAGES) + 1;
        DataPersistanceManager.getInstance().setMaxNumberOfPages(mMaxNumberOfPages, context);
    }

    /**
     * Gets max number of pages that can be retrieved from API
     *
     * @return the int value representing max number of pages
     */
    public int getMaxNumberOfPages(Context context) {
        return DataPersistanceManager.getInstance().getMaxNumberOfPages(context);
    }
}
