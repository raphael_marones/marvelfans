package com.devandroid.raphaelmarones.marvelfanzone.presenters;

import android.content.Context;
import com.devandroid.raphaelmarones.marvelfanzone.views.DetailedView;


public interface DetailedPresenter {

    void attachView(DetailedView view, Context context);
    void onMarvelCharacterFavoritePressed();
}
