package com.devandroid.raphaelmarones.marvelfanzone.interactors;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;

import java.util.List;

public interface HeroInteractor {

    void findHeroes(int offset, int limit, HeroInteractorListener listener, Context mContext);
    List<MarvelCharacters> getFavoriteList(Context mContext);
    void saveFavoriteCharacters(List<MarvelCharacters> marvelCharacters, Context mContext);
    void saveDetailedItem(MarvelCharacters marvelCharacters, Context context);

    interface HeroInteractorListener {
        void onHeroesFound(int page, List<MarvelCharacters> results, boolean isLastPage);
        void onGeneralError();
        void onConnectionError();
        void onLastPageBeingLoaded();
        void onLastPageAlreadyLoaded();
    }
}
