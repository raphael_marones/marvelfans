package com.devandroid.raphaelmarones.marvelfanzone.views;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;

import java.util.List;

public interface SearchView {

    void updateRecyclerView(List<MarvelCharacters> items, List<MarvelCharacters> favoriteCharactersList);
    void setupContent();
    void hideLoading();
    void showLoading();
    void showDetailedActivity();
}
