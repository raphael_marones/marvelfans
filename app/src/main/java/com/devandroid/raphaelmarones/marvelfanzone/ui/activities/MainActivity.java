package com.devandroid.raphaelmarones.marvelfanzone.ui.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.devandroid.raphaelmarones.marvelfanzone.IActivityCallback;
import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.ActivityMainBinding;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.ToolbarCustomBinding;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.MainPresenter;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.MainPresenterImpl;
import com.devandroid.raphaelmarones.marvelfanzone.ui.fragments.FavoriteFragment;
import com.devandroid.raphaelmarones.marvelfanzone.ui.fragments.HeroFragment;
import com.devandroid.raphaelmarones.marvelfanzone.ui.fragments.SearchFragment;
import com.devandroid.raphaelmarones.marvelfanzone.views.MainView;


public class MainActivity extends AppCompatActivity implements MainView, IActivityCallback {

    private MainPresenter mEventHandler;
    private ActivityMainBinding mBinding;
    private ToolbarCustomBinding mToolbarCustomBinding;
    private FavoriteFragment mCurrentFragment;
    private HeroFragment mHeroFragment;
    private FavoriteFragment mFavoriteFragment;
    private SearchFragment mSearchFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mEventHandler = new MainPresenterImpl();
        mEventHandler.attachView(this, this);
    }

    @Override
    public void setupViewElements() {
        mBinding.navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void setupInitialFragment() {
        openHeroFragment();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    openHeroFragment();
                    return true;
                case R.id.navigation_favorites:
                    openFavoriteFragment();
                    return true;
                case R.id.navigation_search:
                    openSearchFragment();
                    return true;

                default:
                    openHeroFragment();
            }
            return false;
        }
    };

    private void openSearchFragment() {
        mSearchFragment = SearchFragment.newInstance();
        switchContent(mSearchFragment);
    }

    private void openFavoriteFragment() {
        mFavoriteFragment = null;
        mFavoriteFragment = (FavoriteFragment) FavoriteFragment.newInstance();
        switchContent(mFavoriteFragment);


    }

    private void openHeroFragment() {
        mHeroFragment = null;
        mHeroFragment = (HeroFragment) HeroFragment.newInstance();
        switchContent(mHeroFragment);

    }

    private void switchContent(final Fragment fragment) {
        final FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_container, fragment);
        fragmentTransaction.commit();
    }
}
