package com.devandroid.raphaelmarones.marvelfanzone.managers;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.Constants;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.persistence.AppPreferences;

import java.util.ArrayList;
import java.util.List;

public class DataPersistanceManager {

    static DataPersistanceManager instance;
    private MarvelCharacters mDetailedItem;

    public static DataPersistanceManager getInstance() {
        if (instance == null) {
            synchronized (DataPersistanceManager.class) {
                if (instance == null) instance = new DataPersistanceManager();
            }
        }
        return instance;
    }

    /**
     * Persist List of MarvelCharacter objects in SharedPreferences in pages
     *
     * @param results List of MarvelCharacter objects
     * @param page  Number of page to be used as key for sharedPreferences:
     * @param context
     */
    public void persistResults(List results, int page, Context context) {
        String pageString = String.valueOf(page);
        AppPreferences.getInstance(context).saveMarvelCharacterList(pageString, results);

        List<MarvelCharacters> allData = getAllData(context);
        if (allData == null) allData = new ArrayList<>();
        allData.addAll(results);
        saveAllData(allData, context);
    }

    /**
     * Persist List of MarvelCharacter objects in SharedPreferences as One Whole
     *
     * @param allData List of MarvelCharacter objects
     * @param context
     */
    private void saveAllData(List<MarvelCharacters> allData, Context context) {
        AppPreferences.getInstance(context).saveMarvelCharacterList(String.valueOf(0), allData);
    }

    public List<MarvelCharacters> getAllData(Context context) {
        return AppPreferences.getInstance(context).getMarvelResultsOnKey(0);
    }

    public List<MarvelCharacters> getPageContent(int page, Context context) {

        return AppPreferences.getInstance(context).getMarvelResultsOnKey(page);
    }

    public List<MarvelCharacters> getFavoriteContent(Context context) {
        List<MarvelCharacters> favoriteList = AppPreferences.getInstance(context).getFavorites();

        if (favoriteList != null) return favoriteList;

        return new ArrayList<MarvelCharacters>();
    }

    public void saveFavoriteList(List<MarvelCharacters> marvelCharacters, Context context) {
        AppPreferences.getInstance(context).saveMarvelCharacterList(Constants.DATABASE.FAVORITE_LIST, marvelCharacters);
    }

    public void setMaxNumberOfPages(int mMaxNumberOfPages, Context context) {
        AppPreferences.getInstance(context).saveMaxPages(Constants.DATABASE.PAGES, mMaxNumberOfPages);
    }

    public int getMaxNumberOfPages(Context context) {
        return AppPreferences.getInstance(context).getMaxPages(Constants.DATABASE.PAGES);
    }

    public void saveDetailedItem(MarvelCharacters item, Context mContext) {
        mDetailedItem = item;
        setupItemFavorite(mContext);
    }

    private void setupItemFavorite(Context mContext) {
        for (MarvelCharacters marvelCharacters: getFavoriteContent(mContext)){
            if (marvelCharacters.getId() == mDetailedItem.getId()){
                mDetailedItem.setFavorite(true);
                break;
            }
        }
    }

    public MarvelCharacters getDetailedItem() {
        return mDetailedItem;
    }
}
