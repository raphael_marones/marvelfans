package com.devandroid.raphaelmarones.marvelfanzone.interactors;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.DtoDataContainer;
import com.devandroid.raphaelmarones.marvelfanzone.entities.DtoResponse;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.managers.DataPersistanceManager;
import com.devandroid.raphaelmarones.marvelfanzone.services.CharacterService;
import com.devandroid.raphaelmarones.marvelfanzone.services.base.BaseCallback;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class SearchInteractorImpl implements SearchInteractor {

    @Override
    public void findHeroesWithName(String query, SearchInteractorListener listener, Context context) {
        List<MarvelCharacters> filteredResults = new ArrayList<>();
        if (query != null && !query.isEmpty()) {
            filteredResults = filterFromOfflineData(query, context);
        }

        List<MarvelCharacters> finalFilteredResults = filteredResults;
        CharacterService.findHeroesQuerySearch(query, new BaseCallback() {
            @Override
            public void onResponse(Call call, Response response) {
                try {
                    DtoResponse dtoCharacterHeroesList = (DtoResponse) response.body();
                    DtoDataContainer dtoDataContainer = dtoCharacterHeroesList.getData();
                    List results = dtoDataContainer.getResults();
                    if (results != null) results.addAll(finalFilteredResults);
                    listener.onHeroesFound(results);
                } catch (Exception e) {
                    Timber.e("[SearchInteractorImpl] - findHeroesWithName -  onResponse : " + e.getMessage());
                    listener.onGeneralError();
                }
            }

            @Override
            public void onGeneralError(Call call, Throwable t) {

            }
        });
    }

    private List<MarvelCharacters> filterFromOfflineData(String query, Context context) {
        List<MarvelCharacters> filteredList = new ArrayList<>();
        for (MarvelCharacters marvelCharacters : DataPersistanceManager.getInstance().getAllData(context)){
            if (marvelCharacters.getName().toLowerCase().contains(query.toLowerCase())) filteredList.add(marvelCharacters);
        }
        return filteredList;
    }

    @Override
    public void saveFavoriteCharacters(MarvelCharacters marvelCharacter, Context mContext) {

    }

    @Override
    public void saveItemDEtailed(MarvelCharacters marvelCharacter, Context context) {
        DataPersistanceManager.getInstance().saveDetailedItem(marvelCharacter, context);
    }
}
