package com.devandroid.raphaelmarones.marvelfanzone.ui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.AdapterFavoriteItemBinding;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelComicSummary;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelStorySummary;
import com.devandroid.raphaelmarones.marvelfanzone.utils.AnimationUtil;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;
import timber.log.Timber;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {

    private LayoutInflater mLayoutInflater;
    private List<MarvelCharacters> mFavoriteList = new ArrayList<>();
    private Action1<List<MarvelCharacters>> onMarvelCaracterFavoritePressed;
    private Action1<MarvelCharacters> onMarvelCaracterItemPressed;

    private Context mContext;

    public FavoriteAdapter(List<MarvelCharacters> list, Context context) {
        mFavoriteList = list;
        mContext = context;
    }

    @NonNull
    @Override
    public FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(viewGroup.getContext());
        }

        AdapterFavoriteItemBinding binding =
                DataBindingUtil.inflate(mLayoutInflater, R.layout.adapter_favorite_item, viewGroup, false);

        return new FavoriteAdapter.FavoriteViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteViewHolder holder, int i) {

        MarvelCharacters item = mFavoriteList.get(i);
        String imagePath = item.getThumbnail().getImagePath();
        String description = item.getDescription();
        holder.heroItemBinding.heroName.setText(item.getName());

        if (description != null && !description.isEmpty())
            holder.heroItemBinding.heroDescription.setText(item.getDescription());
        else if (item.getMarvelComicSummary().getItems().size() > 0) {
            StringBuilder builder = new StringBuilder();

            builder.append(String.format(mContext.getResources().getString(R.string.label_found_in_comics), item.getName()));
            int count = 0;
            for (MarvelComicSummary marvelComicSummary : item.getMarvelComicSummary().getItems()) {
                if (count >= 3) break;
                builder.append(" ");
                if (i > 0) builder.append("  -  ");
                builder.append(marvelComicSummary.getName());
                count++;
            }

            holder.heroItemBinding.heroDescription.setText(builder.toString());
        } else if (item.getMarvelStorySummary().getItems().size() > 0) {
            StringBuilder builder = new StringBuilder();

            builder.append(String.format(mContext.getResources().getString(R.string.label_found_in_stories), item.getName()));
            int count = 0;
            for (MarvelStorySummary marvelStorySummary : item.getMarvelStorySummary().getItems()) {
                if (count >= 3) break;
                builder.append(" ");
                if (count > 0) builder.append("  -  ");
                builder.append(marvelStorySummary.getName());
                count++;
            }

            holder.heroItemBinding.heroDescription.setText(builder.toString());
        } else
            holder.heroItemBinding.heroDescription.setText(String.format(mContext.getString(R.string.label_notfound_at_all), item.getName()));

        try {
            if (imagePath != null && !imagePath.contains("image_not_available"))
                Glide.with(mContext)
                        .load(imagePath)
                        .into(holder.heroItemBinding.heroImage);
        } catch (Exception e) {
            Timber.e("[HeroAdapter] - onBindViewHolder - image loading failed : " + e.getMessage());
        }

        if (item.isFavorite()) {
            holder.heroItemBinding.favoriteImageOff.setVisibility(View.GONE);
            holder.heroItemBinding.favoriteImageOn.setVisibility(View.VISIBLE);
        }

        holder.heroItemBinding.seeMore.setOnClickListener(view -> {
            onMarvelCaracterItemPressed.call(item);
        });

        holder.heroItemBinding.favoriteImageButton.setOnClickListener(view -> {

            item.setFavorite(false);
            AnimationUtil.setCrossFadeAnimation(holder.heroItemBinding.favoriteImageOn, holder.heroItemBinding.favoriteImageOff);
            mFavoriteList.remove(item);
            onMarvelCaracterFavoritePressed.call(mFavoriteList);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return mFavoriteList.size();
    }

    public void setOnMarvelCaracterFavoritePressed(Action1<List<MarvelCharacters>> onMarvelCharacterFavoritePressed) {
        this.onMarvelCaracterFavoritePressed = onMarvelCharacterFavoritePressed;
    }

    public void setOnItemPressed(Action1<MarvelCharacters> onItemPressed) {
        this.onMarvelCaracterItemPressed = onItemPressed;
    }

    public class FavoriteViewHolder extends RecyclerView.ViewHolder {

        public AdapterFavoriteItemBinding heroItemBinding;

        public FavoriteViewHolder(AdapterFavoriteItemBinding heroItemBinding) {
            super(heroItemBinding.getRoot());

            this.heroItemBinding = heroItemBinding;

        }
    }


}
