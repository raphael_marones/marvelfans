package com.devandroid.raphaelmarones.marvelfanzone.entities;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class MarvelComic implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("thumbnail")
    private ThumbnailResource thumbnail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ThumbnailResource getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ThumbnailResource thumbnail) {
        this.thumbnail = thumbnail;
    }

    protected MarvelComic(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = in.readString();
        thumbnail = in.readParcelable(ThumbnailResource.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeParcelable(thumbnail, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MarvelComic> CREATOR = new Creator<MarvelComic>() {
        @Override
        public MarvelComic createFromParcel(Parcel in) {
            return new MarvelComic(in);
        }

        @Override
        public MarvelComic[] newArray(int size) {
            return new MarvelComic[size];
        }
    };
}
