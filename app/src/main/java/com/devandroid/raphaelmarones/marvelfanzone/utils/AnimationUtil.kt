package com.devandroid.raphaelmarones.marvelfanzone.utils

import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation

import com.devandroid.raphaelmarones.marvelfanzone.Constants

class AnimationUtil {

    companion object {
        /**
         * Sets up the animation which cause the view to change alpha (ghosting image)
         *
         * @param viewOut the view that will be invisible after animation
         * @param viewIn  the view to be visible after the animation
         */
        @JvmStatic fun setCrossFadeAnimation(viewOut: View?, viewIn: View?) {
            if (viewOut != null) {
                val animOut = AlphaAnimation(1.0f, 0.5f)
                animOut.duration = Constants.ANIMATION.DURATION_1X.toLong()
                viewOut.visibility = View.INVISIBLE
                viewOut.startAnimation(animOut)
            }

            if (viewIn != null) {
                val animIn = AlphaAnimation(0.2f, 1.0f)
                animIn.startOffset = Constants.ANIMATION.DURATION_OFFSET.toLong()
                animIn.duration = Constants.ANIMATION.DURATION_1X.toLong()
                animIn.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationStart(animation: Animation) {
                        if (viewOut != null) {
                            viewOut.visibility = View.INVISIBLE
                        }
                    }

                    override fun onAnimationEnd(animation: Animation) {}

                    override fun onAnimationRepeat(animation: Animation) {}
                })

                viewIn.visibility = View.VISIBLE
                viewIn.alpha = 1f
                viewIn.startAnimation(animIn)
            }
        }
    }


}
