package com.devandroid.raphaelmarones.marvelfanzone.views;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;

import java.util.List;

public interface HeroView {
    void showLoading(int loading_information);
    void hideLoading();
    void updateMainRecyclerView(List<MarvelCharacters> list, String pageFeedBack, List<MarvelCharacters> favoriteList);
    void setupView();
    void setupWarningToast(int loading_information_last_page);
    void showDetailedActivity();
}
