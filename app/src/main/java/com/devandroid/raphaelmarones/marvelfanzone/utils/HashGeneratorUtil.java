package com.devandroid.raphaelmarones.marvelfanzone.utils;

import java.security.MessageDigest;

import timber.log.Timber;

public class HashGeneratorUtil {

    /**
     * Sets up the Hash for Api calls
     *
     * @param timestamp the long to be used in MD5 algorithm
     * @param privateKey  the String key provided by Api to be used in MD5 algorithm
     * @param publicKey  the String key provided by Api to be used in MD5 algorithm
     */
    public static String generateHash(long timestamp, String privateKey, String publicKey) {

        try {
            String value = timestamp + privateKey + publicKey;
            MessageDigest md5Encoder = MessageDigest.getInstance("MD5");
            byte[] md5Bytes = md5Encoder.digest(value.getBytes());

            StringBuilder md5 = new StringBuilder();
            for (int i = 0; i < md5Bytes.length; ++i) {
                md5.append(Integer.toHexString((md5Bytes[i] & 0xFF) | 0x100).substring(1, 3));
            }

            return md5.toString();
        } catch (Exception e) {
            Timber.e("[HashGeneratorUtil] - generateHash : " + e.getLocalizedMessage());
            return null;
        }
    }
}
