package com.devandroid.raphaelmarones.marvelfanzone.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.devandroid.raphaelmarones.marvelfanzone.Constants;
import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.utils.DelayUtil;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        int delayTime = Constants.ENVIRONMENT.SPLASH_DURATION;
        DelayUtil.delay(delayTime, () -> {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        });
    }

}
