package com.devandroid.raphaelmarones.marvelfanzone.views;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;

import java.util.List;

public interface FavoriteView {
    void updateMainRecyclerView(List<MarvelCharacters> list, String pageFeedBack);
    void setupFeedBackText(String pageFeedBask);
    void showDetailedActivity();
}
