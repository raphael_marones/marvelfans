package com.devandroid.raphaelmarones.marvelfanzone.ui.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.FragmentHeroBinding;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.HeroPresenter;
import com.devandroid.raphaelmarones.marvelfanzone.presenters.HeroPresenterImpl;
import com.devandroid.raphaelmarones.marvelfanzone.ui.activities.DetailedActivity;
import com.devandroid.raphaelmarones.marvelfanzone.ui.adapters.HeroAdapter;
import com.devandroid.raphaelmarones.marvelfanzone.views.HeroView;

import java.util.List;

public class HeroFragment extends BaseFragment implements HeroView {

    private HeroPresenter mEventHandler;
    private FragmentHeroBinding mBinding;
    private HeroAdapter mHeroAdapter;

    public static Fragment newInstance() {
        return new HeroFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_hero, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEventHandler = new HeroPresenterImpl();
        mEventHandler.attachView(this, getContext());
    }

    @Override
    public void onResume() {
        super.onResume();

        mEventHandler.onFavoriteRefreshRequested();
    }

    @Override
    public void setupView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        mBinding.heroRecyclerView.setLayoutManager(layoutManager);
        mBinding.heroRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mBinding.nextPageButton.setOnClickListener(view -> mEventHandler.onNextPageButtonPressed());

        mBinding.previousPageButton.setOnClickListener(view -> mEventHandler.onPreviousPageButtonPressed());
    }

    @Override
    public void setupWarningToast(int loading_information_last_page) {
        Toast.makeText(getContext(), loading_information_last_page, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDetailedActivity() {
        getActivity().startActivity(new Intent(getActivity().getApplicationContext(), DetailedActivity.class));
    }

    @Override
    public void showLoading(int loading_information) {
        if (mActivityCallback == null) return;
//        mActivityCallback.showLoading();
        mBinding.customLoading.setVisibility(View.VISIBLE);
        mBinding.lodingInformation.setText(loading_information);

    }

    @Override
    public void hideLoading() {
        if (mActivityCallback == null) return;
//        mActivityCallback.hideLoading();
        mBinding.customLoading.setVisibility(View.GONE);

    }

    @Override
    public void updateMainRecyclerView(List<MarvelCharacters> list, String pageFeedBack, List<MarvelCharacters> favoriteList) {
        mHeroAdapter = new HeroAdapter(list, favoriteList, getActivity().getApplicationContext());
        mHeroAdapter.setOnMarvelCaracterFavoritePressed(marvelCharacters -> mEventHandler.onMarvelCharacterFavoritePressed(marvelCharacters));
        mHeroAdapter.setOnMarvelCaracterItemPressed(marvelCharacters -> mEventHandler.onItemClicked(marvelCharacters));
        mBinding.heroRecyclerView.setAdapter(mHeroAdapter);
        mBinding.contenInformation.setText(pageFeedBack);
    }
}
