package com.devandroid.raphaelmarones.marvelfanzone.presenters;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.views.FavoriteView;

import java.util.List;

public interface FavoritePresenter {

    void attachView(FavoriteView view, Context context);
    void onMarvelCharacterFavoritePressed(List<MarvelCharacters> marvelCharacters);
    void dettachView();
    void onItemPressed(MarvelCharacters item);
    void refreshFavorites();
}
