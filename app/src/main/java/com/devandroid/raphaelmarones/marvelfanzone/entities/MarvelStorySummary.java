package com.devandroid.raphaelmarones.marvelfanzone.entities;

import com.google.gson.annotations.SerializedName;

public class MarvelStorySummary {

    @SerializedName("resourceURI")
    private String resourceURI;

    @SerializedName("name")
    private String name;

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
