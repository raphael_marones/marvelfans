package com.devandroid.raphaelmarones.marvelfanzone.ui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.AdapterHeroItemBinding;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelComicSummary;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelStorySummary;
import com.devandroid.raphaelmarones.marvelfanzone.utils.AnimationUtil;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;
import timber.log.Timber;

public class HeroAdapter extends RecyclerView.Adapter<HeroAdapter.HeroViewHolder> {

    private LayoutInflater mLayoutInflater;
    private List<MarvelCharacters> mList;
    private List<MarvelCharacters> mFavoriteList = new ArrayList<>();
    private Action1<List<MarvelCharacters>> onMarvelCaracterFavoritePressed;
    private Action1<MarvelCharacters> onMarvelCaracterItemPressed;

    private Context mContext;

    public HeroAdapter(List<MarvelCharacters> list, List<MarvelCharacters> favoriteList, Context context) {
        mList = list;
        mFavoriteList = favoriteList;
        mContext = context;
    }

    public class HeroViewHolder extends RecyclerView.ViewHolder {
        private AdapterHeroItemBinding heroItemBinding;

        private HeroViewHolder(AdapterHeroItemBinding heroItemBinding) {
            super(heroItemBinding.getRoot());

            this.heroItemBinding = heroItemBinding;
        }
    }

    @NonNull
    @Override
    public HeroViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(viewGroup.getContext());
        }
        AdapterHeroItemBinding binding =
                DataBindingUtil.inflate(mLayoutInflater, R.layout.adapter_hero_item, viewGroup, false);

        return new HeroViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull HeroViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        MarvelCharacters item = mList.get(position);
        String imagePath = item.getThumbnail().getImagePath();
        String description = item.getDescription();
        holder.heroItemBinding.heroName.setText(item.getName());

        try {
            if (imagePath != null && !imagePath.contains("image_not_available"))
                Glide.with(mContext)
                        .load(imagePath)
                        .into(holder.heroItemBinding.heroImage);
        } catch (Exception e) {
            Timber.e("[HeroAdapter] - onBindViewHolder - image loading failed : " + e.getMessage());
        }

        if (description != null && !description.isEmpty())
            holder.heroItemBinding.heroDescription.setText(item.getDescription());
        else if (item.getMarvelComicSummary().getItems().size() > 0) {
            StringBuilder builder = new StringBuilder();

            builder.append(String.format(mContext.getResources().getString(R.string.label_found_in_comics), item.getName()));
            int i = 0;
            for (MarvelComicSummary marvelComicSummary : item.getMarvelComicSummary().getItems()) {
                if (i >= 3) break;
                builder.append(" ");
                if (i > 0) builder.append("  -  ");
                builder.append(marvelComicSummary.getName());
                i++;
            }

            holder.heroItemBinding.heroDescription.setText(builder.toString());
        } else if (item.getMarvelStorySummary().getItems().size() > 0) {
            StringBuilder builder = new StringBuilder();

            builder.append(String.format(mContext.getResources().getString(R.string.label_found_in_stories), item.getName()));
            int i = 0;
            for (MarvelStorySummary marvelStorySummary : item.getMarvelStorySummary().getItems()) {
                if (i >= 3) break;
                builder.append(" ");
                if (i > 0) builder.append("  -  ");
                builder.append(marvelStorySummary.getName());
                i++;
            }

            holder.heroItemBinding.heroDescription.setText(builder.toString());
        } else holder.heroItemBinding.heroDescription.setText(String.format(mContext.getString(R.string.label_notfound_at_all), item.getName()));

        if (item.isFavorite()){
            holder.heroItemBinding.favoriteImageOff.setVisibility(View.GONE);
            holder.heroItemBinding.favoriteImageOn.setVisibility(View.VISIBLE);
        } else {
            holder.heroItemBinding.favoriteImageOff.setVisibility(View.VISIBLE);
            holder.heroItemBinding.favoriteImageOn.setVisibility(View.GONE);
        }

        holder.heroItemBinding.seeMore.setOnClickListener(view -> {
            onMarvelCaracterItemPressed.call(item);
        });
        holder.heroItemBinding.favoriteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (item.isFavorite()) {
                    AnimationUtil.setCrossFadeAnimation(holder.heroItemBinding.favoriteImageOn,holder.heroItemBinding.favoriteImageOff);
                    int count = 0;
                    for (MarvelCharacters marvelCharacters : mFavoriteList){
                        if (marvelCharacters.getId() == item.getId()) {
                            mFavoriteList.remove(count);
                            break;
                        }
                        count++;
                    }
                } else {
                    AnimationUtil.setCrossFadeAnimation(holder.heroItemBinding.favoriteImageOff,holder.heroItemBinding.favoriteImageOn);
                    mFavoriteList.add(item);
                }

                item.setFavorite(!item.isFavorite());
                onMarvelCaracterFavoritePressed.call(mFavoriteList);
            }
        });
    }

    public void setOnMarvelCaracterFavoritePressed(Action1<List<MarvelCharacters>> onMarvelCaracterFavoritePressed) {
        this.onMarvelCaracterFavoritePressed = onMarvelCaracterFavoritePressed;
    }

    public void setOnMarvelCaracterItemPressed(Action1<MarvelCharacters> onItemClicked) {
        onMarvelCaracterItemPressed = onItemClicked;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


}
