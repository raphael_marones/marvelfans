package com.devandroid.raphaelmarones.marvelfanzone.utils

import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters
import com.google.gson.Gson

import java.lang.reflect.Type
import java.util.ArrayList
import java.util.Arrays

import timber.log.Timber

class ParseUtil {

    companion object {

        /**
         * Helps to Parse MarvelCharacter to json
         *
         * @param results the list of elements to be parsed
         */
        @JvmStatic fun createMarvelCharacterContent(results: List<MarvelCharacters>): String? {
            try {
                return Gson().toJson(results)
            } catch (e: Exception) {
                Timber.e("Parse failed in [ParseUtil] - createMarvelCharacterContent : %s", e.localizedMessage)
                return null
            }

        }

        /**
         * Helps to Parse json to MarvelCharacter
         *
         * @param json the json stored to be parsed
         */
        @JvmStatic fun retrieveMarvelCharacters(json: String): List<MarvelCharacters>? {
            try {
                val resultsArray = Gson().fromJson<Array<MarvelCharacters>>(json, Array<MarvelCharacters>::class.java as Type)
                return ArrayList(Arrays.asList(*resultsArray))

            } catch (e: Exception) {

                Timber.e("Parse failed in [ParseUtil] - createMarvelCharacterContent : %s", e.localizedMessage)
                return null
            }

        }
    }
}
