package com.devandroid.raphaelmarones.marvelfanzone.ui.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.devandroid.raphaelmarones.marvelfanzone.R;
import com.devandroid.raphaelmarones.marvelfanzone.databinding.AdapterExtraContentBinding;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelEvent;

import java.util.List;

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ExtraViewHolder> {

    private LayoutInflater mLayoutInflater;

    private List<MarvelEvent> mEventList;
    private Context mContext;

    public EventAdapter(List<MarvelEvent> list, Context context) {
        mEventList = list;
        mContext = context;
    }


    @NonNull
    @Override
    public EventAdapter.ExtraViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (mLayoutInflater == null) {
            mLayoutInflater = LayoutInflater.from(viewGroup.getContext());
        }

        AdapterExtraContentBinding binding =
                DataBindingUtil.inflate(mLayoutInflater, R.layout.adapter_extra_content, viewGroup, false);

        return new EventAdapter.ExtraViewHolder(binding);
    }


    @Override
    public void onBindViewHolder(@NonNull EventAdapter.ExtraViewHolder holder, int i) {
        MarvelEvent item = mEventList.get(i);
        holder.binding.extraTitle.setText(item.getTitle());

        if (item.getDescription() != null && !item.getDescription().isEmpty()) holder.binding.extraDescription.setText(item.getDescription());
        else holder.binding.extraDescription.setText(String.format(mContext.getString(R.string.information_not_loaded), item.getTitle()));

    }

    @Override
    public int getItemCount() {
        return mEventList.size();
    }

    public class ExtraViewHolder extends RecyclerView.ViewHolder {
        AdapterExtraContentBinding binding;

        public ExtraViewHolder(AdapterExtraContentBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }

}
