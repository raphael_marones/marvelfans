package com.devandroid.raphaelmarones.marvelfanzone.interactors;

import android.content.Context;

import com.devandroid.raphaelmarones.marvelfanzone.entities.DtoDataContainer;
import com.devandroid.raphaelmarones.marvelfanzone.entities.DtoResponse;
import com.devandroid.raphaelmarones.marvelfanzone.entities.MarvelCharacters;
import com.devandroid.raphaelmarones.marvelfanzone.managers.DataPersistanceManager;
import com.devandroid.raphaelmarones.marvelfanzone.services.CharacterService;
import com.devandroid.raphaelmarones.marvelfanzone.services.base.BaseCallback;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

public class DetailedInteractorImpl implements DetailedInteractor {

    @Override
    public MarvelCharacters getDetailedItemSaved() {
        return DataPersistanceManager.getInstance().getDetailedItem();
    }

    @Override
    public void findComics(int id, DetailedInteractorListener listener, Context mContext) {
        CharacterService.findComicsByHeroes(id, new BaseCallback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() != 200){
                    listener.onComicsNotFound();
                    Timber.e("[DetailedInteractorImpl] - findComics - FAILED");
                    return;
                }

                try {
                    DtoResponse dtoResponse = (DtoResponse) response.body();
                    DtoDataContainer dtoDataContainer = dtoResponse.getData();
                    List results = dtoDataContainer.getResults();
                    if (!results.isEmpty()) {
                        listener.onComicsFound(results);
                    } else listener.onComicsNotFound();

                } catch (Exception e){

                    listener.onComicsNotFound();
                    Timber.e("[DetailedInteractorImpl] - findComics - FAILED");
                }
            }

            @Override
            public void onGeneralError(Call call, Throwable t) {
                listener.onComicsNotFound();
                Timber.e("[DetailedInteractorImpl] - findComics - FAILED:" + t.getLocalizedMessage());
            }
        });
    }

    @Override
    public void findEvents(int id, DetailedInteractorListener listener, Context mContext) {
        CharacterService.findEventsByHeroes(id, new BaseCallback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() != 200){
                    listener.onEventsNotFound();
                    Timber.e("[DetailedInteractorImpl] - findEvents - FAILED");
                    return;
                }

                try {
                    DtoResponse dtoResponse= (DtoResponse) response.body();
                    DtoDataContainer dtoDataContainer = dtoResponse.getData();
                    List results = dtoDataContainer.getResults();
                    if (!results.isEmpty()) {
                        listener.onEventsFound(results);
                    } else listener.onEventsNotFound();

                } catch (Exception e){

                    listener.onEventsNotFound();
                    Timber.e("[DetailedInteractorImpl] - findEvents - FAILED");
                }

            }

            @Override
            public void onGeneralError(Call call, Throwable t) {

            }
        });
    }

    @Override
    public void findSeries(int id, DetailedInteractorListener listener, Context mContext) {
        CharacterService.findSeriesByHeroes(id, new BaseCallback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() != 200){
                    listener.onSeriesNotFound();
                    Timber.e("[DetailedInteractorImpl] - findSeries - FAILED");
                    return;
                }

                try {
                    DtoResponse dtoResponse= (DtoResponse) response.body();
                    DtoDataContainer dtoDataContainer = dtoResponse.getData();
                    List results = dtoDataContainer.getResults();
                    if (!results.isEmpty()) {
                        listener.onSeriesFound(results);
                    } else listener.onSeriesNotFound();

                } catch (Exception e){

                    listener.onSeriesNotFound();
                    Timber.e("[DetailedInteractorImpl] - findSeries - FAILED");
                }
            }

            @Override
            public void onGeneralError(Call call, Throwable t) {


                listener.onSeriesNotFound();
                Timber.e("[DetailedInteractorImpl] - findSeries - FAILED");
            }
        });
    }

    @Override
    public void findStories(int id, DetailedInteractorListener listener, Context mContext) {
        CharacterService.findStoriesByHeroes(id, new BaseCallback() {
            @Override
            public void onResponse(Call call, Response response) {
                if (response.code() != 200){
                    listener.onStoriesNotFound();
                    Timber.e("[DetailedInteractorImpl] - findComics - FAILED");
                    return;
                }

                try {
                    DtoResponse dtoResponse= (DtoResponse) response.body();
                    DtoDataContainer dtoDataContainer = dtoResponse.getData();
                    List results = dtoDataContainer.getResults();
                    if (!results.isEmpty()) {
                        listener.onStoriesFound(results);
                    } else listener.onStoriesNotFound();

                } catch (Exception e){

                    listener.onStoriesNotFound();
                    Timber.e("[DetailedInteractorImpl] - findStories - FAILED");
                }
            }

            @Override
            public void onGeneralError(Call call, Throwable t) {

                listener.onStoriesNotFound();
                Timber.e("[DetailedInteractorImpl] - findStories - FAILED");
            }
        });
    }

    @Override
    public void ToggleFavoriteOnDetailedItem(Context context) {
        List<MarvelCharacters> favoriteList = DataPersistanceManager.getInstance().getFavoriteContent(context);
        MarvelCharacters detailedItem = DataPersistanceManager.getInstance().getDetailedItem();
        if (!detailedItem.isFavorite()) {
            detailedItem.setFavorite(true);
            favoriteList.add(detailedItem);
            DataPersistanceManager.getInstance().saveFavoriteList(favoriteList, context);
            DataPersistanceManager.getInstance().saveDetailedItem(detailedItem, context);
        }
        else {
            List<MarvelCharacters> favoriteListUpdated = new ArrayList<>();
            for (MarvelCharacters marvelCharacters : favoriteList){
                if (marvelCharacters.getId() != detailedItem.getId()){
                    favoriteListUpdated.add(marvelCharacters);
                }
            }

            detailedItem.setFavorite(false);
            DataPersistanceManager.getInstance().saveFavoriteList(favoriteListUpdated, context);
            DataPersistanceManager.getInstance().saveDetailedItem(detailedItem, context);
        }
    }
}
