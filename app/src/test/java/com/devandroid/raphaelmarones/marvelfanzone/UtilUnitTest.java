package com.devandroid.raphaelmarones.marvelfanzone;

import com.devandroid.raphaelmarones.marvelfanzone.utils.HashGeneratorUtil;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UtilUnitTest {

    @Test
    public void hashGeneratorTest() {

        String ptKey = BuildConfig.PRIVATE_KEY;
        String pbKey = BuildConfig.PUBLIC_KEY;
        long ts = 100000;

        String hash = HashGeneratorUtil.generateHash(ts, ptKey, pbKey);


        assertEquals(hash, "7c04fdce7547fdf0b481acc180f827e3");
    }
}