
# MARVEL FAN ZONE


## Overview
Application mainly implemented using Java, with some Utils using Kotlin
Android Studio 3.3 
Min Sdk 21
Tgt Sdk 28


## LIBRARIES USED
- Gson - This library was used for parsing JSON as it  can save lots of time spent writing parsing code, I always use it because it supports arbitrarily complex objects
- RxJava/RxAndroid - For Actions, it is a gap in my skills, need to study it more to use to the best of its possibilities 
- Glide - For image loading I used Glide over Picasso, even though Picasso save some space on the cache, Glide's usually faster as it keeps images previously resized
- Android dataBinding - Must say it was only my second time using it, and it is an amazing way to perform the bonds, even though i didn't use it to its full potential, its simplicity makes the code amazingly cleaner the Butterknife, almost Kotlin like.
- Retrofit - This library was used for Networking, it helps with boiler plate code and performs quite well
- Timber - Logging lib, I personally use it for its auto Tagging properties
## ARCHITECTURE
- The architecture was VIPER based as it allows the clear distinction of layers to enable separation of concerns trying to respect SOLID principles.
